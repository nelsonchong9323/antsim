
---
## A basic ant colony simulation implemented in javascript. 

Open public/index.html to run the demo in your browser. 
Script for the ant simulation is in public/ants.js.

To-do:
1. Optimize performance!
2. Implement walls/tunnels!
3. Decorate the simulator!
4. Clean up the code!

---
