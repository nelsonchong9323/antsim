document.addEventListener('DOMContentLoaded', function () {
    initAntSim();
    let particle = new Particles();
    particle.init();
});

// toggle display for food canvas
let foodButton = document.getElementById('toggleFood');
foodButton.addEventListener('click', function() {
    let foodCanvas = document.getElementById('foodcanvas');
    foodCanvas.classList.toggle("d-none");
    foodButton.classList.toggle("btn-primary");
    foodButton.classList.toggle("btn-outline-secondary");
});

// toggle display for ant canvas
let antButton = document.getElementById('toggleAnts');
antButton.addEventListener('click', function() {
    let overlayCanvas = document.getElementById('overlaycanvas');
    overlayCanvas.classList.toggle("d-none");
    antButton.classList.toggle("btn-primary");
    antButton.classList.toggle("btn-outline-secondary");
});

// toggle light/dark theme for trail canvas
// let bgButton = document.getElementById('toggleDark');
// bgButton.addEventListener('click', function() {
//     let game = document.getElementById('scrollContainer');
//     game.classList.toggle("dark");
//     bgButton.classList.toggle("btn-primary");
//     bgButton.classList.toggle("btn-outline-secondary");
// });

// function to convert html elements into images
// different capture function based on browser compatibility
let scButton = document.getElementById('takeScreenshot');
scButton.addEventListener('click', function() {
    let parser = new UAParser();
    console.log(parser.getResult().ua);

    if (parser.getResult().ua.includes("Safari")) {
        console.log("Safari");
        html2canvas(document.querySelector("#game > *")).then(canvas => {
            document.querySelector("#screenModal .modal-body").innerHTML = "";
            document.querySelector("#screenModal .modal-body").appendChild(canvas);
        });
    } 
    else {
        domtoimage.toPng(document.querySelector("#game > *")).then(function (dataUrl) {
            var img = new Image();
            img.src = dataUrl;
            document.querySelector("#screenModal .modal-body").innerHTML = "";
            document.querySelector("#screenModal .modal-body").appendChild(img);
        }).catch(function (error) {
            console.error('oops, something went wrong!', error);
        });
    }
});

// ==============================================================================
// variables and functions to set up recording in webm

var elementToRecord = document.getElementById('scrollContainer');
var canvas2d = document.getElementById('background-canvas');
var context = canvas2d.getContext('2d');

canvas2d.width = elementToRecord.clientWidth;
canvas2d.height = elementToRecord.clientHeight;

var isRecordingStarted = false;
var isStoppedRecording = false;

var recorder;
var startRecordButton = document.getElementById('startRecord');
var stopRecordButton = document.getElementById('stopRecord');

startRecordButton.addEventListener('click', function() {
    recorder = new RecordRTC(canvas2d, { type: 'canvas', mimetype: "video/webm;codecs=h264", frameRate: 30 });
    startRecordButton.classList.toggle("d-none");
    stopRecordButton.classList.toggle("d-none");
    isStoppedRecording = false;
    isRecordingStarted = true;

    (function looper() {
        if(!isRecordingStarted) { return setTimeout(looper, 500); }
    
        html2canvas(elementToRecord).then(function(canvas) {
            context.clearRect(0, 0, canvas2d.width, canvas2d.height);
            context.drawImage(canvas, 0, 0, canvas2d.width, canvas2d.height);
    
            if(isStoppedRecording) { return; }
            requestAnimationFrame(looper);
        });
    })();
    
    recorder.startRecording();
    document.getElementById('recordCounter').classList.toggle("d-none");
});

stopRecordButton.addEventListener('click', function() {
    startRecordButton.classList.toggle("d-none");
    stopRecordButton.classList.toggle("d-none");
    
    recorder.stopRecording(function() {
        document.getElementById('recordCounter').classList.toggle("d-none");
        isRecordingStarted = false;
        isStoppedRecording = true;
        var blob = recorder.getBlob();
        var preview = document.getElementById('preview-video');
        preview.src = URL.createObjectURL(blob);
        document.querySelector("#screenModal .modal-body").innerHTML = "";
        document.querySelector("#screenModal .modal-body").appendChild(preview);

    });
});


